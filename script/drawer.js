// const all = d3.select(".all");
// const mainboard = d3.select(".mainboard");
let textbox = document.getElementsByClassName("textbox")[0];

const sleep = (ms) => new Promise(resolve => setTimeout(resolve,ms));

const readTime = (length) => {
    if (length <= 21) {
        return 500 + 40 * length / 6;
    }else {
        return Math.pow(length,3) * 640 / Math.pow(21,3);
    }
    return length/10 * 1000;
};

async function showTextByInterval(text,textbox){
    console.log(text);
    document.title = text.title;
    for (const line of text.contents){
        const len = line[1].length;
        const sleep_time = readTime(len);
        textbox.innerHTML = line[1];
        await sleep(sleep_time);
    }
}


const process = (root, summary_file) => 
      fetch(summary_file)
      .then(response => response.json())
      .then(async summary => {
          const start = Math.floor(Math.random() * summary.length);
          const len = summary.length;
          const ordering = summary
                .slice(start,len)
                .concat(summary.slice(0,start));
          console.log(ordering);
          for (const filename of ordering){
              await fetch(`${root}/${filename}`)
                  .then(response => response.json())
                  .then(text => showTextByInterval(text,textbox));
          }})
      .then(ok => console.log("OK"), ng => console.log(ng));

