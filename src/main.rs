extern crate serde;
extern crate serde_json;
use serde::{Deserialize, Serialize};
use std::fs::File;
use std::io::{Read, Write};
use std::path::Path;

#[derive(Debug, Serialize, Deserialize)]
struct Text {
    title: String,
    contents: Vec<(usize, String)>,
}

fn calc_reading_time(sentence: &str) -> usize {
    let len = sentence.chars().count();
    if len == 0 {
        500
    } else {
        100 * len
    }
}

fn get_sentences(file: std::path::PathBuf) -> std::io::Result<Text> {
    let mut file = File::open(&file)?;
    let mut input = String::new();
    file.read_to_string(&mut input)?;
    let mut input = input.lines();
    let title = input.next().unwrap().to_string();
    let contents = input
        .map(|e| {
            if e.starts_with('「') || e.ends_with('」') {
                vec![e.to_string()]
            } else if e.trim().is_empty() {
                vec![]
            } else {
                e.split_terminator('。')
                    .map(|e| e.to_string())
                    .map(|mut e| {
                        e.push('。');
                        e
                    })
                    .collect()
            }
        })
        .flat_map(|e| e)
        .map(|e| (calc_reading_time(&e), e))
        .collect();
    let text = Text { title, contents };
    Ok(text)
}

fn main() -> std::io::Result<()> {
    let args: Vec<_> = std::env::args().collect();
    let input_dir = &args[1];
    let output_root = &args[2];
    let summary_file = &args[3];
    let mut files = vec![];
    for file in std::fs::read_dir(&Path::new(input_dir))?
        .filter_map(|e| e.ok())
        .filter(|e| e.path().is_file())
    {
        let file_stem = file
            .path()
            .file_stem()
            .and_then(|e| e.to_str())
            .unwrap()
            .to_string();
        files.push(format!("{}.txt", &file_stem));
        let sentences = get_sentences(file.path())?;
        let filename = format!("{}{}.txt", output_root, file_stem);
        let mut output = File::create(Path::new(&filename))?;
        let out = serde_json::ser::to_string_pretty(&sentences)?;
        writeln!(&mut output, "{}", out)?;
        output.flush()?;
    }
    let mut output = File::create(Path::new(&summary_file))?;
    writeln!(
        &mut output,
        "{}",
        serde_json::ser::to_string_pretty(&files).unwrap()
    )?;
    Ok(())
}
